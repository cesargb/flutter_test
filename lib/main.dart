import 'package:flutter/material.dart';

import 'food_list.dart';
import 'number_list_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: MainMenuList(),
      routes: routes,
    );
  }

  final  routes = <String,WidgetBuilder> {
    'number_list' : (BuildContext ctx) => RamdomNumbers(),
    'foot_list' : (ctx) => Foodlist(),
  };




}

class MainMenuList extends StatefulWidget {
  @override
  MainMenuListState createState() => MainMenuListState();

}


class MainMenuListState extends State<MainMenuList> {
  final pages = ["number_list","foot_list"];
  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(title: Text("Test App"),),
    body: ListView.builder(
        padding:const EdgeInsets.all(16.0),
        itemCount:pages.length ,
        itemBuilder: (c,i) => ListTile(
          title: Text(pages[i]),
          onTap: ()=> Navigator.of(c).pushNamed(pages[i]),
        )
    ),
  );
}
