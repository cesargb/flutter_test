


import 'dart:math';

import 'package:flutter/material.dart';


class Foodlist extends StatefulWidget{

  @override
  State createState()  => _FoodlistState();
}

class _FoodlistState extends State<Foodlist> {
  List<FoodModel> foodlist = [

  ];


  @override
  void initState() {

      super.initState();
      _initList();
  }

  void _initList() async{
    foodlist = [
      FoodModel(1,"Tacos","https://www.cocinavital.mx/wp-content/uploads/2017/08/como-hacer-tacos-de-suadero-estilo-callejero-en-casa-.jpg"),
      FoodModel(1,"Burritos","https://www.comedera.com/wp-content/uploads/2017/08/receta-burritos-mexicanos.jpg"),
      FoodModel(1,"Tostadas","https://www.superama.com.mx/views/micrositio/recetas/images/fiestaspatrias/tostadastinga/Web_fotoreceta.jpg"),
      FoodModel(1,"Sopes","https://cdn2.cocinadelirante.com/sites/default/files/filefield_paths/sopes_de_carne.jpg"),
      FoodModel(1,"Enchiladas","https://cocina-casera.com/mx/wp-content/uploads/2017/09/enchiladas-verdes-1.jpg"),
    ];
  }
  @override
  Widget build(BuildContext context)
  => Scaffold(
    appBar: AppBar(title: Text("Comida"),),
    body:  ListView.builder(itemCount: foodlist.length, itemBuilder: (ctx,i) => _buildRow(foodlist[i], i,ctx)),
  );

  Widget _buildRow(FoodModel model, int index,BuildContext ctx)
  => Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column( children: <Widget>[

          Image.network(model.url),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
            Text(model.name),
            RaisedButton(child: Text("-"),onPressed: (){setState(() {
              if(model.counter<=0) return;
              model.counter -=1;
            });}),
            Text(model.counter.toString()),
            RaisedButton(child: Text("+"),onPressed: (){setState(() {
              if(model.counter>=20) return;
              model.counter +=1;
            });})
          ]),

        ],)
      ),
    );
}


class FoodModel{
  int id;
  String name;
  String url;
  int counter = 0;
  FoodModel(this.id,this.name,this.url);

}

