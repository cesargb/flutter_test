

import 'dart:math';

import 'package:flutter/material.dart';

class RamdomNumbers extends StatefulWidget {
  @override
  RamdomNumbersState createState() => RamdomNumbersState();
}

class RamdomNumbersState extends State<RamdomNumbers> {
  final _number_list = <int>[];
  final _font = const TextStyle(fontSize: 18.0);
  String _title = "Press a  number";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_title),
      ),
      body: ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemBuilder: (ctx,i){
          if(i.isOdd) return Divider();
          final index = i ~/ 2;
          if(index>=_number_list.length){
            _number_list.addAll(_generateNumbers(10));
          }
          return _buildRow(_number_list[index]);

        },
      ),
    );

  }

  List<int> _generateNumbers(int take){
    final _random = new Random();
    final fnn = (int min, int max) => min + _random.nextInt(max - min);

    var l = List<int>();
    for( var i = 0 ; i <= take; i++ ) {
      l.add(fnn(1000,10000));
    }
    return l;// Text(fnn(1000,10000).toString());

  }







  Widget _buildRow(int number){
    return ListTile(
      title: Text(number.toString(),style: _font,),
      onTap: () => _updateTitle(number),
    );
  }
  _updateTitle(int number) => setState(() {
    _title = number.toString();
  });

}

